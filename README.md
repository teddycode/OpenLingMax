# 开发者神器库.exe



语言: 中文

官网: www.lingmax.top

平台: winXP/win7/win8/win10    (Mac和Linux参考Wine方案)

下载地址: https://pan.baidu.com/s/14Bz45GuCyL7lSiZANmT0xw#list/path=%2FOpenLingMax

### 技术栈

NodeJs语言 + NW.js内核 + layui界面框架 +  webkit通用技术 + sqlite3数据库 + JQ框架 + CodeMirror编辑器

### 功能简介

开发者神器库是一款集合 全栈开发者 常用编程工具的仓库。图片处理、HTTPS抓包改包、DNS优化、逆向工具、日常办公、等等一系列提高工作效率的工具......

### 原创功能如下

 **网络抓包改包**  : 方便 **逆向调试** DeBUG，各种 **解禁功能** 等等

 **HTTP请求**  : 支持拼音搜索快速启动、可以模拟发送http Api表单(独家支持POST视图显示)、 **自定义参数验证签名** 

 **文本处理**  : 轻松批量手工处理50万字符操作处理, 并且支持编码解码、 **正则子匹配拆分显示** 、HTML转离线(静态文件转Base64Url)

 **搜索快启动**  : 拼音搜索支持软件、网址、POST接口、脚本，显示隐藏 窗口快捷键【Alt+Z】焦点默认在搜索框里!

 **软件仓库**  : 支持拼音搜索快速启动、开发者常用到的 **工具神器的绿色版** ,下载即用!无繁琐步骤! 并且支持添加删除

 **脚本仓库**  : 支持拼音搜索快速启动、开机脚本、BAT脚本、CMD命令、Node脚本、以及 **JQ脚本(支持ajax)** 

 **常用网站**  ：支持拼音搜索快速启动、支持浏览器收藏夹的网站导入、

 **DNS加速**  : 访问国外文档网站提速, 杜绝运营商DNS故障, 多服务器网站提速

 **DNS优化**  ： 测试服与正式服HOST无缝切换、广告域名修改、支持手机修改、翻译不能访问等等

 **HTTP302跳转**  : 【HTTP镜像多线程下载加速, 本地302访问跳转, 镜像加速专用】



.

### 文档教程

[https://gitee.com/xhxx/OpenLingMax/wikis/pages?sort_id=1553094&doc_id=364869](https://gitee.com/xhxx/OpenLingMax/wikis/pages?sort_id=1553094&doc_id=364869)

### 视频教程下载

[https://pan.baidu.com/s/14Bz45GuCyL7lSiZANmT0xw](https://pan.baidu.com/s/14Bz45GuCyL7lSiZANmT0xw)

![教程](http://jay.lingmax.top/img/2020320143138.png "教程")



.

####  QQ交流群 61746131 

如果群号搜索不了 请点击加群链接：[QQ群61746131](https://shang.qq.com/wpa/qunwpa?idkey=0c393cdd8bf9ac94bc1955badfb62d7ef28d56137c0082394d2c55887680ab1c)


## 拼音搜索快速启动 快捷键显示隐藏【Alt+Z】

 **拼音搜索**  支持首拼搜索，空格分隔关键字（关键字倒过来也支持匹配）

【拼音=py】【拼音=p yin】【拼音=pin y】【音拼=pin y】【音拼=p y】【拼音=pinyin】

![拼音搜索快速启动](https://images.gitee.com/uploads/images/2020/0522/223448_391ea6b6_1387857.gif "GIF.gif")


## DNS优化 访问国外文档网站提速, 杜绝运营商DNS故障, 多服务器网站提速

 **DNS修改**  测试服与正式服HOST无缝切换  支持手机设置

 **DNS加速** 获取域名的所有服务器, 选出延迟最低的当解析结果。国外  **微软文档、github、npm**  访问加速。 与传统DNS加速本质上不同, 传统加速只是选一个比较快的DNS使用。

![DNS加速](https://images.gitee.com/uploads/images/2020/0317/211159_76daf0dd_1387857.gif "DNS加速6.gif")

.

## 网络抓包

HTTPS代理[ 抓包/改包 ]  以及 [ UDP/TCP/ICMP ]抓包监控

支持: 修改HTTPS数据包 跳转实现 测试服与正式服HOST无缝切换

返回伪造结果、正则子匹配替换结果、修改服务器IP、URL跳转重定向、拦截访问、可选 转发与不转发 代理请求

![网络抓包](https://images.gitee.com/uploads/images/2020/0317/211228_1e9f576c_1387857.gif "网络抓包6.gif")

.

## HTTP请求 发送http Api表单(独家支持POST视图显示)、 **自定义参数验证签名脚本**

![HTTP请求](https://images.gitee.com/uploads/images/2020/0317/211310_934130b6_1387857.gif "HTTP请求6.gif")

.

## 正则搜索匹配(支持子匹配拆开)  文本处理 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0326/143524_ec1b5d39_1387857.png "正则表达式.png")

.

## HTML转独立资源 并且支持各种编码解码

![HTML转独立资源](https://images.gitee.com/uploads/images/2020/0317/211335_1ecc20d7_1387857.gif "HTML转独立资源6.gif")

.


2020年5月22日
#### 开发者神器库7

变动：快捷键显示隐藏【Alt+Z】, HTTP代理抓包支持配置端口，网站图标显示, 软件图标显示 , DNS优化改成TCP查询

增加： 拼音快速搜索启动->支持搜索【软件、网址、POST接口、自定义脚本】。

HTTP302跳转【HTTP镜像多线程下载加速,  本地302访问跳转, 镜像加速专用】

.


2020年3月17日
#### 开发者神器库6

变动：DNS修改、DNS放行、网络抓包、HTTP请求多窗口模式、HTML转独立离线资源、子匹配文本正则语句显示

增加： HTTPS代理[ 抓包/改包 ]  以及 [ UDP/TCP/ICMP ]抓包监控

支持: 修改HTTPS数据包 跳转实现 测试服与正式服HOST无缝切换、返回伪造结果、正则子匹配替换结果、修改服务器IP、URL跳转重定向、拦截访问、选择性代理请求

#### window配合(Proxifier软件全系统强制走代理), 即可任意软件HTTPS抓包改包，与Fiddler(FD)一样, 记得配置Proxifier过滤放行NBuilder.exe的任何请求，否则 代理就走死循环了。 或者用虚拟机 隔开抓包软件和Proxifier环境

.


2020年1月17日

#### 开发者神器库5

增加DNS加速， 服务器用80端口或支持ping，且有多个服务器的才能加速（ 原理：选众多服务器中最快那个 ）

必须设DNS为127.0.0.1备用DNS设114.114.114.114。UDP53端口不能被占用，关闭本软件后不会断网。(软件有一键设置按钮 但可能设不成功)

- 访问全世界的大型网站都加速,例如: github,微软文档,国外文档网站 gu歌drive下载 等....(不支持翻墙 谷歌搜索与其他被禁的网站) 
- 支持部分大型网络软件的客户端加速 skype 国外邮箱 等等...
- 配合每天开机启动 加速电脑网络非常不错! [已经确认]国外 微软文档 github npm 访问速度加快了


.



2019年6月5日

#### 开发者神器库4

正则文本处理、编码转换、POST请求、自动化脚本-----发起HTTP请求, 可以保存接口, 自定义HTTP自动化脚本 支持验签.....

.


2019年1月15日

#### 开发者神器库2.0

多功能小脚本、命令库-----存放常用的命令 [随手记录的sql shell cmd vbs node npm...]

.



2018年12月13日

#### 开发者神器库1.0

教程榜、工具合集 [网络工具,必备工具,激活工具,声音处理,图片处理,...]

常用网站合集 [网页在线工具,省钱活动榜,各大框架文档,...]


.


#### 开发者神器库--单机版

本项目本着开放性研究 / 实验使用。如有侵犯您的权利,敬请告知。xiaohxx@qq.com

.


#### 二次开发必看

LingMax    目录是 代码文件目录

NW.js      目录是 NW.js_0.14.7_SDK 版, Node版本是 **5.12** 

1.拉取下来直接可以执行  **开发者工具箱.exe**  来启动项目 (这软件就执行了一句 启动命令行而已)

2.npm插件 **已经编译**  所以 项目自带node_modules目录  **直接运行即可** , **不需要用npm** 下载了,

3.软件窗口 鼠标右键 > 点击  **检查[调试webkit内核]**  按钮  **检查背景[调试NodeJs]**   弹出 软件调试工具

4.本软件采用模块化开发 一般一个页面加载的文件就几个 (代码可能比较长 建议 **折叠div** /function再调试)

## 参考API文档 -- 无需熟悉API也能二开 需要哪些功能直接看手册就行

NodeJs环境API                   http://nodejs.cn/api/    注意这个Node版本是5.12(为了支持XP)

Chrome浏览器JavaScript环境API   https://www.runoob.com/js/js-tutorial.html   当然MDN的更全.但是内容驳杂难懂

jQuery插件API                   https://www.runoob.com/jquery/jquery-tutorial.html

NW.js环境API                    https://nwjs.org.cn/doc/index.html

layui界面框架                   https://www.layui.com/doc/element/form.html
