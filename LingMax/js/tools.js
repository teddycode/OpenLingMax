/**
 * 工具箱模块
 * 
 * by:LingMax
 */
var iixc = 0; //异步的坑

// HOOK  遇到开发者环境时
var dirxx = dir;
if(cwd == 'G:\\syn\\OpenLingMax\\LingMax'){
    console.log('工具库切换到 开发者环境');
    dirxx = function(){
        var aacce = arguments;
        if(aacce.length > 0 && (aacce[0]+'').length > 0) aacce[0] = 'G:/app/LingMax/resource/'+aacce[0];
        return dir.apply(this, aacce);
    };
}else if(cwd == 'D:\\syn\\OpenLingMax\\LingMax'){
    console.log('工具库切换到 开发者环境 公司');
    dirxx = function(){
        var aacce = arguments;
        if(aacce.length > 0 && (aacce[0]+'').length > 0) aacce[0] = 'D:/OpenLingMax/resource/'+aacce[0];
        return dir.apply(this, aacce);
    };
}


mkdirsSync('data/userData/img');


/**
 * 重载界面
 * @param {json} json_tools  数据
 * @param {dom} dom_pant   父元素 的js原生元素
 */
function local_tools(json_tools, dom_pant) {

    var zjj = function(jx) {
        var lj, addr, iis, ls = '';
        var ll = dirxx(json_tools[jx].path);
        lj = fs.existsSync(ll);
        //console.log(!json_tools[jx].ico,json_tools[jx].path,(json_tools[jx].ico+'').length);
        /*
        //自动加载图标
        if(!json_tools[jx].ico){
            //console.log(json_tools[jx].path);
            var p = ll;
            var pimg ='data/userData/img/'+cryptoX.createHash('sha256').update(p).digest('hex')+'_LingMax.png';
            var iimg = path.resolve(pimg);
            cmd('"'+path.resolve('data/LingMaxTools.exe')+'"'+ ' icon "'+p+'" "'+iimg+'" 1 2 3 4 5',()=>{},()=>{
                //console.log(2222,json_tools[jx].id,json_tools[jx].path);
                try {
                    var ico = fs.readFileSync(iimg);
                    db.run('UPDATE obj set ico =? where id =?',[ 'data:image/x-icon;base64,'+ico.toString('base64'),json_tools[jx].id]);
                } catch (error) {
                    console.log(error);
                }
               
                fs.unlink(pimg, (err) => {});
            });
        }
        */

        // console.log(fs.existsSync(ll+'_LingMax.png'),ll+'_LingMax.png',getIcon);
        //    var pimg ='data/userData/img/'+cryptoX.createHash('sha256').update(json_tools[jx].path).digest('hex')+'_LingMax.png';
        //     var iimg =path.resolve(pimg);
        //     if (!fs.existsSync(iimg)){
        //         cmd('"'+path.resolve('data/LingMaxTools.exe')+'"'+ ' icon "'+ll+'" "'+iimg+'" 1 2 3 4 5');
        //     }
        //  pimg = '/'+pimg ;
        var pimg = json_tools[jx].ico?json_tools[jx].ico:'/img/rj.png';
        addr = json_tools[jx].id;
        //edit_info(id)
        ls = '<div class="layui-collapse" ><div class="layui-colla-item"><h2 class="layui-colla-title" style="background-image: url(\''+pimg+'\')">' + json_tools[jx].name + '<div class="py_right" data-path="' + addr + '"><a href="javacript:;" title="打开主程序目录" data-path="' + addr + '" class="mainpathx ' + (lj ? '' : 'layui-hide') + '"><i class="layui-icon layui-icon-app color_qj_green"></i></a> <a href="javacript:;" title="打开下载目录" data-path="' + addr + '" class="pathx ' + (lj ? '' : 'layui-hide') + '"><i class="layui-icon layui-icon-template-1 color_qj_green"></i></a><a href="javacript:;" title="修改详情" class="webedit"><i class="layui-icon color_qj_green layui-icon-edit"></i></a> <a href="javacript:;" title="去网站下载" data-path="' + addr + '" class="webpath "><i id="xhrun" class="layui-icon color_qj_green ' + (lj ? 'layui-icon-ok' : 'layui-icon-upload') + '"></i></a> <button class="layui-btn layui-btn-normal btn_run execx" data-path="' + addr + '">' + (lj ? '运行' : '下载') + '</button> </div></h2><div class="layui-colla-content"> <div class=" kkide"></div><p>' + json_tools[jx].recommend + '</p>';
        ls = ls + '</div></div></div>';
        iis = $(ls);
        dom_pant.append(iis);
        iis = $(iis.find(".kkide")[0]);
        iixc++;
        db.all('select * from obj where su = ' + addr + ' order by sortx desc', [], function(err, rows) {
            //console.log(iis);
            local_tools(rows, iis);
            iixc--;
            if (iixc == 0) {
                csh();
            }
        });

    };
    for (var jx in json_tools) {
        zjj(jx);
    }
}